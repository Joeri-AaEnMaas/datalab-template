#!/usr/bin/env python

"""Package entry point."""


import pandas as pd

from datalab_template_python.gui import main


if __name__ == '__main__':  # pragma: no cover
    print("Hello World!")
    targets = [(('make', 'test-unit', 'DISABLE_COVERAGE=true'), "Unit Tests", True),(('make', 'test-all'), "Integration Tests", False),(('make', 'check'), "Static Analysis", True),(('make', 'docs'), None, True),]