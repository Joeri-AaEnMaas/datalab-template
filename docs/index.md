# Overview

Template used for Python projects

This project was generated with [cookiecutter](https://github.com/audreyr/cookiecutter) using [jacebrowning/template-python](https://github.com/jacebrowning/template-python).

[![Unix Build Status](https://img.shields.io/travis/JoeriFresen/datalab-template-python/master.svg?label=unix)](https://travis-ci.org/JoeriFresen/datalab-template-python)
[![Windows Build Status](https://img.shields.io/appveyor/ci/JoeriFresen/datalab-template-python/master.svg?label=windows)](https://ci.appveyor.com/project/JoeriFresen/datalab-template-python)
[![Coverage Status](https://img.shields.io/coveralls/JoeriFresen/datalab-template-python/master.svg)](https://coveralls.io/r/JoeriFresen/datalab-template-python)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/JoeriFresen/datalab-template-python.svg)](https://scrutinizer-ci.com/g/JoeriFresen/datalab-template-python/?branch=master)
[![PyPI Version](https://img.shields.io/pypi/v/DatalabTemplatePython.svg)](https://pypi.org/project/DatalabTemplatePython)
[![PyPI License](https://img.shields.io/pypi/l/DatalabTemplatePython.svg)](https://pypi.org/project/DatalabTemplatePython)

# Setup

## Requirements

* Python 3.8+

## Installation

Install it directly into an activated virtual environment:

```text
$ pip install DatalabTemplatePython
```

or add it to your [Poetry](https://poetry.eustace.io/) project:

```text
$ poetry add DatalabTemplatePython
```

# Usage

After installation, the package can imported:

```text
$ python
>>> import datalab_template_python
>>> datalab_template_python.__version__
```
